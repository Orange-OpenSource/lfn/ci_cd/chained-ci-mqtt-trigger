#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""
    Service Queue for storing event
    =====================================
"""
from six.moves.queue import Queue, Empty

class ServiceQueue():
    """
        Service Queue for storing event
        =====================================

        Handles a queue with events that can be retrieved and get
    """
    def __init__(self):
        self._events = Queue()

    def get_event(self, block=True, timeout=None):
        """ Get the next event from the queue.

        :param block: Set to True to block if no event is available.
        :param timeout: Timeout to wait if no event is available.
        :type block: bool
        :type timeout: int

        :return: The next event as a JSON Data
            instance, or nothing if:
             - `block` is False and there is no event available in the queue, or
             - `block` is True and no event is available within the time
               specified by `timeout`.

        :Example:
        >>> sq = ServiceQueue()
        >>> sq.put_event({'a': 1, 'b': 2})
        >>> sq.get_event()
        {'a': 1, 'b': 2}
        >>> sq.get_event(timeout=1)
        >>> sq.get_event(block=False)
        >>> sq.put_event({'a': 2, 'b': 3})
        >>> sq.get_event(block=False)
        {'a': 2, 'b': 3}
        """
        try:
            return self._events.get(block, timeout)
        except Empty:
            return None

    def put_event(self, data):
        """ Create event from `data` and add it to the queue.

        :param data: The JSON data.
        :type date: json

        :Raises: :class:`six.moves.queue.Full` if the queue is full.

        :Example:
        >>> sq = ServiceQueue()
        >>> sq.put_event({'a': 1, 'b': 2})
        >>> sq.put_event({'a': 2, 'b': 3})
        >>> sq.get_event()
        {'a': 1, 'b': 2}
        """
        self._events.put(data)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
