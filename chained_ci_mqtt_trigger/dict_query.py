#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""
    Utilities for chained CI MQTT Trigger
    =====================================

    some simple functions used in the different classes
"""

class DictQuery(dict):
    """
        Dictionnary query helper
        ========================
    """
    def get(self, path, default=None):
        """
            Deep retrieve a value in nested dictionnaries
            :param self: this class, used for self call
            :param path: the path of the value to get with dots ('.') between
                          elements
            :param default: what to give back if not found
            :return: The value of the element in path

            :Example:

            >>> dictionnary = {'a': {'b': {'c':['d', 'e']}}}
            >>> DictQuery(dictionnary).get('a.b.c')
            ['d', 'e']
            >>> DictQuery(dictionnary).get('a.b.d', default='NotFound')
            'NotFound'
        """
        keys = path.split(".")
        val = None

        for key in keys:
            if val:
                if isinstance(val, list):
                    val = [v.get(key, default) if v else None for v in val] # pylint: disable=E1133
                else:
                    val = val.get(key, default)
            else:
                val = dict.get(self, key, default)

            if not val:
                break

        return val

if __name__ == "__main__":
    import doctest
    doctest.testmod()
