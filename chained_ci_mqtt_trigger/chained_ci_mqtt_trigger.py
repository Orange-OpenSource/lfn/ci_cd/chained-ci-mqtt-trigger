#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import argparse
import logging
import logging.config
import sys
import json
import pkg_resources

from six.moves import configparser as ConfigParser
import paho.mqtt.client as mqtt

from gitlab_client import GitlabClient
from notification_client import NotificationClient
from service_queue import ServiceQueue

logging.config.fileConfig(
    pkg_resources.resource_filename('chained_ci_mqtt_trigger','logging.ini'))
logging.captureWarnings(True)
logger = logging.getLogger("chained_ci_mqtt_trigger")

def _get_options():
    parser = argparse.ArgumentParser(
        description="listen to a MQTT Topic and trigger a Chained CI")
    parser.add_argument('conffile', nargs=1, help="Configuration file")
    return parser.parse_args()

def on_connect_publish(_client, _userdata, _flags, rc):
    logger.info("connected to MQTT Broker with status %s for publishing",
                str(rc))

def on_connect_subscribe(client, userdata, _flags, rc):
    logger.info("connected to MQTT Broker with status %s for subscribing",
                str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    logger.info("subscribing on topic %s with qos %s",
                userdata['topic'], str(userdata['qos']))
    client.subscribe(("{}/#".format(userdata['topic']), userdata['qos']))

def on_message(_client, userdata, msg):
    logger.debug("received a message")
    topic = userdata['topic']
    magic_word = userdata['magic_word']
    trigger_on_comment = userdata['trigger_on_comment']
    logger.debug("parent topic: %s", topic)
    logger.debug("topic received: %s", msg.topic)
    subtopics = userdata['subtopics']
    for subtopic in subtopics:
        logger.debug("checking with subtopic %s", subtopic)
        # we may trigger a chained-ci in 2 cases
        # * topic = gerrit/myproject
        #   subtopic=patchset-created
        #   subtopic=comment-added with a specific comment "gating_recheck"

        if msg.topic == "{}/{}".format(topic, subtopic):
            logger.info("Got a message with a right topic: %s", msg.topic)
            queue = userdata['queue']
            # if subtopic == comment-added, check the comment
            launch_gitlab = True
            if trigger_on_comment in msg.topic:
                logger.debug(
                    "verifying that magic word %s is present in comment %s",
                    magic_word, json.loads(msg.payload)["comment"])
            if (trigger_on_comment in msg.topic and
                    magic_word not in json.loads(msg.payload)["comment"]):
                # do not execute
                # standard comment shall not trigger chained-ci
                launch_gitlab = False
            if launch_gitlab:
                queue.put_event(str(msg.payload.decode('UTF-8')))

def _configure_mqtt(config, name, on_connect_callback,
                    on_message_callback=None, userdata=None):
    logger.debug("configure and connect MQTT client %s", name)
    # Configure MQTT
    mqtt_port = config.getint('mqtt', 'port', fallback=1883)
    mqtt_keepalive = config.getint('mqtt', 'keepalive', fallback=60)
    mqtt_transport = config.get('mqtt', 'transport', fallback="tcp")


    ## Configure auth
    auth = None
    mqtt_username = config.get('mqtt', 'username', fallback=None)
    mqtt_password = config.get('mqtt', 'password', fallback=None)
    if mqtt_username:
        auth = {'username': mqtt_username}
        if mqtt_password:
            auth['password'] = mqtt_password

    client = mqtt.Client(transport=mqtt_transport)
    client.on_connect = on_connect_callback
    if on_message_callback:
        client.on_message = on_message_callback
    if userdata:
        client.user_data_set(userdata)
    if mqtt_transport == "websockets" and config.has_option('mqtt', 'ws_path'):
        client.ws_set_options(path=config.get('mqtt', 'ws_path'), headers=None)
    client.enable_logger(logging.getLogger(name))
    client.connect(config.get('mqtt', 'hostname'), mqtt_port, mqtt_keepalive)
    return client

def _parse_dict_variables(array_variables):
    return {
        item.split('=')[0]:item.split('=')[1] for item in
            [v for v in array_variables.
                split('\n') if v is not '']}

def main():
    args = _get_options()
    config = ConfigParser.RawConfigParser()
    config.optionxform = lambda option: option
    config.read(args.conffile)

    mqtt_qos = config.getint('mqtt', 'qos', fallback=0)

    # retrieve the subtopics where we'll react on
    subtopics = [
        v for v in config.get('mqtt', 'subtopics').split('\n') if v is not '']

    # retrieve the variables we always want to put in gitlab request
    additional_variables = _parse_dict_variables(
        config.get('gitlab',
                   'additional_variables', fallback=''))
    notification_additional_variables = _parse_dict_variables(
        config.get('notification',
                   'additional_variables', fallback=''))
    private_tokens = _parse_dict_variables(
        config.get('gitlab',
                   'private_tokens', fallback=''))

    summary_console_jobs = _parse_dict_variables(
        config.get('gitlab',
                   'summary_console_jobs', fallback=''))

    logger.debug("build additional variables: %s", additional_variables)
    logger.debug("notification additional variables: %s",
                 notification_additional_variables)

    # retrieve the transformations we want to perform in order to give the
    # moving variables
    transformations = {k:v for k, v in config.items('transformations')}
    logger.debug("transformations: %s", transformations)
    notification_tranformations = {k:v for k, v in config.items(
        'notification_tranformations')}
    logger.debug("notification_tranformations: %s", notification_tranformations)

    logger.debug("Service Queue creation")
    queue = ServiceQueue()

    logger.debug("MQTT publisher client creation")
    publish_client = _configure_mqtt(config, "mqtt.client.publisher",
                                     on_connect_publish)

    logger.debug("MQTT publisher creation")
    notification_client = NotificationClient(
        publish_client,
        config.get('mqtt', 'notification_topic'),
        config.get('gitlab', 'nickname', fallback="Generic Gitlab"),
        notification_tranformations,
        additional_variables=notification_additional_variables)

    logger.debug("gitlab ci client creation")
    gitlab = GitlabClient(
        config.get('gitlab', 'hostname'),
        config.get('gitlab', 'chained_ci_project_id'),
        config.get('gitlab', 'project_token'),
        private_tokens,
        config.get('gitlab', 'git_reference'),
        notification_client,
        additional_variables=additional_variables,
        transformations=transformations,
        summary_console_jobs=summary_console_jobs)

    logger.debug("MQTT subscriber client creation")
    suscriber_userdata = {
        'topic': config.get('mqtt', 'topic'), 'subtopics': subtopics,
        'qos': mqtt_qos, 'queue': queue,
        'magic_word': config.get('mqtt', 'magic_word', fallback="recheck"),
        'trigger_on_comment': config.get('mqtt', 'trigger_on_comment',
                                         fallback="comment-added"),
    }
    subscribe_client = _configure_mqtt(config, "mqtt.client.subscriber",
                                       on_connect_subscribe,
                                       on_message_callback=on_message,
                                       userdata=suscriber_userdata)

    publish_client.loop_start()
    subscribe_client.loop_start()
    while True:
        try:
            logger.debug("trying to get a new event")
            data = queue.get_event()
            logger.debug("new event found, launching gitlab")
            gitlab.launch(data)
        except KeyboardInterrupt:
            logger.debug("shutdown asked")
            break

if __name__ == "__main__":
    main()
