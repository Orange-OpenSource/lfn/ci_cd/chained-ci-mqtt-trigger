#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""
    NotificationClient
    ==================
"""
import json
import logging

from dict_query import DictQuery

class NotificationClient(): # pylint: disable=too-few-public-methods
    """
        NotificationClient
        ==================

        Object used to send message into a MQTT topic

        :param client: the MQTT client to use to send the notification
        :param notification_topic: the MQTT topic to use
        :param nickname: the nickame of the sender. Use by listeners of the
                         topic to know from who the message is
        :param notification_transformations: dictionary giving how to retrieve
                                             dynamic variables that will be sent
                                             in the messages
        :param additional_variables: variables that we'll always send in the
                                     message

        :type client: MQTT client instance (from paho.mqtt)
        :type notification_topic: string
        :type nickname: string
        :type notification_transformations: dict
        :type additional_variables: dict, default to {}

        :Example:

        >>> notification_transformations = {
                'gerrit_review': 'change.number',
                'gerrit_patchset': 'patchSet.number'}
        >>> client = NotificationClient(mqtt_client,
                                        "gerrit/reviews",
                                        "my_chained_ci_tester",
                                        notification_transformations,
                                        {'the_answer': 42})
    """
    # pylint: disable=too-many-arguments
    def __init__(self, client, notification_topic, nickname,
                 notification_transformations, additional_variables=None):
        self._client = client
        self._topic = notification_topic
        self._nickname = nickname
        self._transformations = notification_transformations
        self._additional_variables = additional_variables or {}
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.debug("client initialized")

    def notify(self, message, payload, score=0):
        """
            Send a notification on the MQTT topic

            :param message: the message to send
            :param payload: JSON where we can retrieve dynamic variables that
                            will be retrieved thanks to
                            notification_transformations
            :param score: the score set in the message

            :type message: string
            :type payload: JSON
            :type score: int, default to 0

            :Example:
            >>> notification_transformations = {
                    'gerrit_review': 'change.number',
                    'gerrit_patchset': 'patchSet.number'}
            >>> client = NotificationClient(mqtt_client,
                                            "gerrit/reviews",
                                            "my_chained_ci_tester",
                                            notification_transformations,
                                            {'the_answer': 42})
            >>> payload ={'change': {'number': 42},
                          'patchSet': {'number': '1664'}}
            >>> client.notify("hello", payload)
            will send the following message to "gerrit/review" topic:
                {
                    "message": "hello",
                    "from": "my_chained_ci_tester",
                    "score": 0,
                    "the_answer": 42,
                    "gerrit_review": 42,
                    "gerrit_patchset": "1664"
                }
            >>> client.notify("hello", "yolo")
            will send nothing as "yolo" is not a dict
        """
        self.log.debug("sending notification")
        self.log.debug("message: %s", message)
        self.log.debug("payload: %s", payload)
        self.log.debug("score: %s", score)

        try:
            values = json.loads(payload)
            self.log.debug("retrieving additional_variables")
            message_payload = {}
            message_payload.update(self._additional_variables)

            for variable, transformation in self._transformations.items():
                message_payload[variable] = DictQuery(values).get(
                    transformation)

            message_payload.update({"message": message, "score": score,
                                    "from": self._nickname})

            self.log.debug("message to be sent: %s",
                           json.dumps(message_payload))
            published = self._client.publish(self._topic,
                                             json.dumps(message_payload))
            self.log.debug("publication status: %s, published: %s",
                           published.rc, published.is_published())
            published.wait_for_publish()
            self.log.debug("notification sent")
        except ValueError as exception:
            self.log.exception(exception)
