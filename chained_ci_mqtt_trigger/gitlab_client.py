#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""
    GitlabClient
    ============
"""
import json
import logging
import time
import re

import requests

from dict_query import DictQuery

class GitlabClient():
    """
        GitlabClient
        ============

        Object used to interact with Gitlab and Chained-CI

        :param hostname: the FQDN of gitlab instance
        :param chained_ci_id: the ID of Chained-CI project on this Gitlab
                              instance
        :param project_token: the "trigger token" used to launch a pipeline on
                              Chained-CI project
        :param private_tokens: a dict containing the private_tokens to all
                               gitlab instances we may want to connect to
        :param git_reference: the branch of Chained-CI we want to use
        :param notification_client: the client used for notification.
        :param additional_variables: variables we want to send to Chained_CI
                                     pipeline trigger
        :param transformations: dictionary giving how to retrieve dynamic
                                variables that will be sent to Chained-CI
                                pipeline trigger
        :param wait_time: how many seconds we wait between two check of the
                          status of the pipeline
        :param max_retries: how many checks do we do before deciding the
                            pipeline is too long.
        :param max_timeout_retries: how many check do we accept with timeout
                                    (i.e. we cannot connect to gitlab instance)
        :param summary_console_jobs: dic with the names of inner jobs of
                                     Chained-CI jobs where we can retrieve
                                     information we want to give back to the
                                     MQTT answer

        :type hostame: string of a base url ('http://example.com)
        :type chained_ci_id: int
        :type project_token: string
        :type private_tokens: dict of string with FQDN as key and private token
                              for this FQDN as value
        :type git_reference: string
        :type notification_client: NotificationClient or any object with same
                                   functions
        :type additional_variables: dict, default to {}
        :type transformations: dict, default to {}
        :type wait_time: int, default to 10
        :type max_retries: int, default to 1080
        :type max_timeout_retries: int, default to 5
        :type summary_console_jobs: dict, default to {}

        :Example:

        >>> transformations = {
                'GERRIT_REVIEW': 'change.number',
                'GERRIT_PATCHSET': 'patchSet.number'}
        >>> private_tokens = {'gitlab.com': 'mysecret_token',
                              'another.fqdn': 'another_secret'}
        >>> summary_console_jobs = {'second_step': 'pages',
                                    'third_step': 'deploy'}
        >>> client = GitlabClient("https://gitlab.com", 1234, secret_token,
                                  private_tokens, "master", notification_client,
                                  additional_variables={'MYVAR': 42},
                                  transformations=transformations,
                                  summary_console_jobs=summary_console_jobs)


        :seealso: https://docs.gitlab.com/ce/ci/triggers/README.html
        :seealso: https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html
        :seealso: NotificationClient
        :note: the summary_console_jobs may be commplex to understand.
               Here's an example:
               let's say your chained-ci pipeline is the following:
               | first_step | --> | second_step | --> | third_step |

               second_step is launching the following pipeline:
               | test | --> | deploy | --> | pages |
               third_step is launching the following pipeline:
               | deploy | --> | notify |

               if summary_console_jobs is the following:
                   {'second_step': 'pages', 'third_step': 'deploy'}
               that means that if second step fails, we'll try to retrieve
               (part of) the output of "pages" job of second_step launched
               pipeline.
               And if third_steps fails, we'll try to retrieve (part of) the
               output of "deploy" job of second_step launched pipeline.


               the part of the output retrieved is every lines written between
               '______________ Results _____________' and
               '____________________________________' in the job console.

    """

    def __init__(self, hostname, chained_ci_id, project_token, private_tokens,
                 git_reference, notification_client, additional_variables={},
                 transformations={}, wait_time=10, max_retries=1080,
                 max_timeout_retries=5, summary_console_jobs={}):
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.debug("client initialization")
        self._project_token = project_token
        self._private_tokens = private_tokens
        self._git_reference = git_reference
        self._notification_client = notification_client
        self._additional_variables = additional_variables
        self._transformations = transformations
        self._base_url = "{}/api/v4/projects/{}".format(hostname,
                                                        chained_ci_id)
        self._wait_time = wait_time
        self._max_retries = max_retries
        self._max_timeout_retries = max_timeout_retries
        self._summary_console_jobs = summary_console_jobs
        self.log.debug("client initialized")

    def launch(self, payload):
        """
            launch a pipeline and wait for the end

            :param payload: JSON where we can retrieve dynamic variables that
                            will be retrieved thanks to transformations

            :Example:

            >>> transformations = {
                'GERRIT_REVIEW': 'change.number',
                'GERRIT_PATCHSET': 'patchSet.number'}
            >>> private_tokens = {'gitlab.com': 'mysecret_token',
                                  'another.fqdn': 'another_secret'}
            >>> summary_console_jobs = {'second_step': 'pages',
                                    'third_step': 'deploy'}
            >>> client = GitlabClient("https://gitlab.com", 1234, secret_token,
                                      private_tokens, "master",
                                      notification_client,
                                      additional_variables={'MYVAR': 42},
                                      transformations=transformations,
                                      summary_console_jobs=summary_console_jobs)
            >>> payload ={'change': {'number': 42},
                          'patchSet': {'number': '1664'}}
            >>> client.launch(payload)
            will trigger a pipeline on project 1234 from gitlab.com with the
            following variables:
                GERRIT_REVIEW = 42
                GERRIT_PATCHSET = '1664'
                MYVAR = 42
            and will wait up to the end or upto 1080 times 10 seconds.
        """
        self.log.debug("received a launch request")
        self._notification_client.notify("Starting Chained-CI build", payload)
        try:
            values = json.loads(payload)
            variables = self._additional_variables

            for variable, transformation in self._transformations.items():
                variables[variable] = DictQuery(values).get(transformation)

            self.log.debug("variables to be sent: %s", json.dumps(variables))
            url = "{}/trigger/pipeline".format(self._base_url)
            request_payload = {'token': self._project_token,
                               'ref': self._git_reference}

            for k, v in variables.items():
                request_payload.update({"variables[{}]".format(k): v})

            self.log.debug("payload to be sent: %s", request_payload)
            self.log.debug("url to use: %s", url)

            request = requests.post(url, data=request_payload)

            self.log.debug("request status code: %s", request.status_code)
            self.log.debug("request response: %s", request.text)

            response = request.json()
            if request.ok:
                self.log.debug("request is OK")

                message = "Chained-CI build started, max time: {} seconds".format(
                    self._max_retries * self._wait_time)
                self._notification_client.notify(message, payload)
                pipeline_id = response["id"]
                self.wait_for_pipeline_end(pipeline_id, payload)
            else:
                self.parse_request_error(request, payload,
                                         "Chained-CI build starting error.")
        except (ValueError, requests.RequestException) as exception:
            self.log.exception(exception)

    def wait_for_pipeline_end(self, pipe_id, payload):
        """
            wait for the end of the pipeline

            :param pipe_id: the ID of the pipeline to monitor
            :param payload: JSON where we can retrieve dynamic variables that
                            will be retrieved thanks to transformations
        """
        self.log.debug("wait for end of pipeline %s", pipe_id)
        retry = 0
        while retry < self._max_retries:
            if self.check_pipeline_status(pipe_id, payload):
                break
            retry += 1
            time.sleep(self._wait_time)
        if retry >= self._max_retries:
            message = "Chained-CI build too long. Aborting."
            self._notification_client.notify(message, payload)

    def check_pipeline_status(self, pipe_id, payload, timeout_retry=0):
        """
            Check the status of a pipeline

            :param pipe_id: the ID of the pipeline to monitor
            :param payload: JSON where we can retrieve dynamic variables that
                            will be retrieved thanks to transformations
            :param timeout_retry: how many timeout we already had
        """
        headers = {'PRIVATE-TOKEN': self._private_token(self._base_url)}
        url = "{}/pipelines/{}".format(self._base_url, pipe_id)

        self.log.debug("url to use: %s", url)
        try:
            request = requests.get(url, headers=headers)
            request.raise_for_status()
            response = request.json()

            self.log.debug("request is OK")

            status = response["status"]
            self.log.debug("pipeline %s status: %s", pipe_id, status)

            if status == "success":
                message = "Chained-CI build finished with status OK"
                self._notification_client.notify(message, payload)
                return True
            if status == "failed":
                self.parse_pipeline_error(pipe_id, payload)
                return True
            return False

        except (ValueError, requests.RequestException) as exception:
            self.log.exception(exception)
            if timeout_retry > self._max_timeout_retries:
                self.parse_request_error(
                    request, payload,
                    "Chained-CI status retrieving error.")
                return True
            time.sleep(self._wait_time)
            timeout_retry += 1
            return self.check_pipeline_status(
                pipe_id, payload,
                timeout_retry=timeout_retry)


    def parse_pipeline_error(self, pipe_id, payload, timeout_retry=0):
        """
            Parse a pipeline error

            :param pipe_id: the ID of the pipeline to monitor
            :param payload: JSON where we can retrieve dynamic variables that
                            will be retrieved thanks to transformations
            :param timeout_retry: how many timeout we already had
        """
        self.log.debug("parsing error in pipeline %s", pipe_id)

        headers = {'PRIVATE-TOKEN': self._private_token(self._base_url)}
        url = "{}/pipelines/{}/jobs".format(self._base_url, pipe_id)
        self.log.debug("url to use: %s", url)
        summary_output = ""

        try:
            request = requests.get(url, headers=headers)
            self.log.debug("request status code: %s", request.status_code)
            request.raise_for_status()

            response = request.json()
            self.log.debug("response: %s", response)

            failed_jobs = self.find_failed_jobs(response)

            for job_id, _value in failed_jobs.items():
                failed_jobs[job_id] = self.parse_job_error(failed_jobs, job_id,
                                                           headers)
            message = "Chained CI build failure on some jobs."
            message += "\nJobs with issue:"

            for _job_id, value in failed_jobs.items():
                if 'output' in value:
                    message += "\n{}".format(value['output'])
                else:
                    message += "\n  * name: {}, no url".format(value["name"])
                if 'summary' in value:
                    summary_output += "\n{}".format(value["summary"])
            self._notification_client.notify(message, payload)
            self._notification_client.notify(summary_output, payload)

        except (ValueError, requests.RequestException) as exception:
            self.log.exception(exception)
            if timeout_retry > self._max_timeout_retries:
                self.parse_request_error(
                    request, payload,
                    "Chained-CI status retrieving error.")
            else:
                time.sleep(self._wait_time)
                timeout_retry += 1
                self.parse_pipeline_error(
                    pipe_id, payload,
                    timeout_retry=timeout_retry)

    def find_failed_jobs(self, response):
        failed_jobs = {}
        for item in response:
            self.log.debug("item %s with name %s has status %s", item['id'],
                           item['name'], item['status'])
            if item["status"] == "failed":
                failed_jobs.update({item["id"]: {"name": item["name"]}})
        self.log.debug("failed_jobs: %s", failed_jobs)
        return failed_jobs

    def parse_job_error(self, failed_jobs, job_id, headers):
        job_info = failed_jobs[job_id]
        trace_url = "{}/jobs/{}/trace".format(self._base_url, job_id)
        self.log.debug("url to use: %s", trace_url)
        request = requests.get(trace_url, headers=headers)
        self.log.debug("trace request status code: %s", request.status_code)
        if request.ok:
            pattern = re.compile(r'\* Step:[\s\S]*[\*]{66}",',
                                 re.MULTILINE)
            output = None
            output_search = pattern.search(request.text)
            if output_search:
                output = output_search.group(0)
            self.log.debug("output: %s", output)
            if output:
                output = re.sub(r'\* Step:',"- Step:", output,
                                flags=re.MULTILINE)
                output = re.sub(r'^[ ]+"', "  ", output,
                                flags=re.MULTILINE)
                output = re.sub(r'",', "", output, flags=re.MULTILINE)
                output = re.sub(r'[\*]{66}', "", output, flags=re.MULTILINE)
                name = job_info["name"]
                job_info["output"] = output
                self.log.debug("checking if job %s has a summary job", name)
                if name in self._summary_console_jobs.keys():
                    job_info["summary"] = self.get_summary(job_info)
                else:
                    self.log.debug("name %s is not in summary_console_jobs %s", name, self._summary_console_jobs)
        return job_info

    def parse_request_error(self, request, payload, message):
        """
            Parse the error of an HTTP request

            :param request: the failed request
            :param payload: JSON where we can retrieve dynamic variables that
                            will be retrieved thanks to transformations
            :param message: the message to send with the request error
        """
        self.log.debug("request is NOK")
        self.log.debug("request status code: %s", request.status_code)
        self.log.debug("request response: %s", request.text)
        response = request.json()
        if request.status_code == 404:
            failure_message = response["error"]
        elif request.status_code == 400:
            failure_message = response["message"]["base"]
        else:
            failure_message = response["message"]
        message += " Status code: {}, failure message: {}".format(
            request.status_code, failure_message)
        self._notification_client.notify(message, payload)

    def get_summary(self, job_info):
        self.log.debug("retrieving summary for job %s", job_info["name"])
        summary_job = self._summary_console_jobs[job_info["name"]]
        self.log.debug("inner_job name: %s", summary_job)
        pipeline_pattern = re.compile(
            r'API pipeline url: \'(?P<pipeline_url>[\S]*)\'')
        pipe_url = None
        pipe_url_output = pipeline_pattern.search(job_info["output"])
        self.log.debug("pipe_url_output: %s", pipe_url_output)
        if pipe_url_output:
            pipe_url = pipe_url_output.group('pipeline_url')
        if pipe_url:
            return self.parse_output(pipe_url, summary_job)
        return ""

    def parse_output(self, url, summary_job):
        headers = {'PRIVATE-TOKEN': self._private_token(url)}
        try:
            self.log.debug("url to use: %s", "{}/jobs".format(url))
            request = requests.get("{}/jobs".format(url), headers=headers)
            self.log.debug("inner pipeline request status code: %s",
                           request.status_code)
            request.raise_for_status()
            response = request.json()
            job_id = None
            for job in response:
                if job["name"] == summary_job:
                    job_id = job["id"]
                    break
            if job_id:
                job_url = re.sub(r'pipelines/[0-9]+',
                                 "jobs/{}/trace".format(job_id),
                                 url)
                self.log.debug("url to use: %s", job_url)
                request = requests.get(job_url, headers=headers)
                request.raise_for_status()
                pattern = re.compile(
                    r'______________ Results _____________[\s\S]*' +
                    r'____________________________________',
                    re.MULTILINE)
                if pattern.search(request.text):
                    return pattern.search(request.text).group(0)
        except (ValueError, requests.RequestException) as exception:
            self.log.exception(exception)
        return ""

    def _private_token(self, url):
        pattern = re.compile(r'http[s]*://(?P<hostname>[^/]*)')
        hostname_search = pattern.search(url)
        if not hostname_search:
            return None
        hostname = hostname_search.group('hostname')
        if hostname not in self._private_tokens:
            self.log.debug("token for %s NOT found", hostname)
            return None
        self.log.debug("token for %s found", hostname)
        return self._private_tokens[hostname]
