# Chained CI MQTT Trigger

Goal of this app is to listen to MQTT topics and trigger a pipeline in
Chained-CI ([<https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-examples>](Example> of Chained CI implementation)).

Chained CI MQTT Trigger is expecting hierarchy of topics like this:

```bash
master_topic
├── project_one
│   ├── comment-added
│   ├── ...
│   └── patchset-created
├── ...
│
└── project_X
    ├── comment-added
    ├── ...
    └── patchset-created
```

Use of[<https://gitlab.com/Orange-OpenSource/lfn/ci_cd/gerrit-to-mqtt>](MQTT to Gerrit)
in order to feed the topics is a good way to use this script.

Chained-CI MQTT Trigger will listen into a topic (`/master_topic/project_one`
for example) and will trigger a pipeline if a new message appears in one the
desired subtopics (`patchset-created` for example). In the case of
added-comment, the magic word **gating_recheck** will be necessary to trigger
the pipeline. It will prevent the launch of a pipeline on any comment. 

It will then monitor the status of the pipeline.

According to the status of the pipeline (success/failure/timeout), a message
will be sent to a MQTT (configured in `notification.topic` in configuration file
) topic with the results. The payload of the message will be a `JSON` dictionary
with this minimum values:


```JSON
{
    "score": "0",
    "from": "the_sender",
    "message": "the message"
}
```

## Configuration

configuration is done via a configuration file
an example is in etc/gerrit-to-mqtt.conf:

```gitconfig
[mqtt]
hostname=localhost
topic=gerrit/myproject
# Only if needed for websockets
ws_path=/path
# transport is "websockets" or "tcp"
# default is "tcp"
transport=websockets
# If qos is not set, default to 0
qos=2
# If port is not set, default to 1883
port=1883
# If you need authentication on the MQTT Broker
username=username
password=password
# The subtopics we want to listen on
subtopics=
  patchset-created
  comment-added

[notification]
topic=onap/reviews

[gitlab]
hostname=https://gitlab.forge.orange-labs.fr
nickname=Gitlab Public
chained_ci_project_id=1234
project_token=_trigger_token_
private_token=_private_user_token_
git_reference=master
additional_variables=
  POD=onap_oom_gating_pod4
  WORKAROUND=False

[transformations]
GERRIT_REVIEW=change.number
GERRIT_PATCHSET=patchSet.number

[notification_tranformations]
gerrit_review=change.number
gerrit_patchset=patchSet.number
```

## Tokens for gitlab_ci

Gitlab defines several tokens.

`project_token` is needed to be able to trigger a pipeline in chained-ci
project. Go to `Settings --> CI/CD --> Pipeline triggers` in chained-ci project
and create one.
`private_token` is (or may be) needed to look at the status of the pipeline. Go
to your profile page and then `Access Tokens`. Create a token with `api` scope
and use it in your configuration.

## Static and Dynamic variables when setting the pipeline

You may (will) need to set variables when triggering you Chain (in particular,
the variable `POD` which allow to choose the deployment).

the item `additional_variables` in `gitlab` part of the configuration part allow
that.

If you want to pass variables which has dynamic value (I want to give an id
retrieced in the MQTT message for example), you'll have to give the
transformation scheme in `transformation` topic.

For example `GERRIT_REVIEW=change.number` will pass the value of the parameter
`number` of the parameter `change`found in the incoming MQTT message and set the
variable `GERRIT_REVIEW` accordingly.

so, let's have this message:
```JSON
{
    "message": "my_awesome_project",
    "change": {
        "name": "a name",
        "number": "12",
        "date": "today"
    },
    "author": "Morgan Richomme"
}
```

with the previous transformation (`GERRIT_REVIEW=change.number`), I'll set in
the pipeline trigger the variable `GERRIT_REVIEW` with value `12`.

## Dynamic variables for notification

Same dynamicity may be needed for the notification part. the same scheme applies
but it needs to be done in `notification_tranformations` part.

## Usage

### Command Line

After installation of the requirements, launch the script with the path of the
configuration file as arguments

```bash
pip install -r requirements.txt
python mqtt-to-gerrit.py etc/mqtt-to-gerrit.conf
```

### Docker

you'll have to put the right path of the id_rsa key in `mqtt-to-gerrit.conf`. In
the example below, it would be `key=/etc/ssh/id_rsa`.

```bash
docker run  --volume path/to/configuration:/etc/mqtt-to-gerrit.conf:ro \
            --volume path/to/id_rsa:/etc/ssh/id_rsa:ro \
    registry.gitlab.com/orange-opensource/lfn/ci_cd/mqtt-to-gerrit:latest
```

## TODO

- [ ] put log directly on output
- [ ] choose log level in the configuration and via the command line
