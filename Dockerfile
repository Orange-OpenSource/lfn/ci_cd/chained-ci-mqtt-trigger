FROM alpine:3.8

ARG PIP_TAG=18.0

WORKDIR /opt/chained-ci-mqtt-trigger

COPY requirements.txt requirements.txt

RUN apk add --no-cache libressl\
                       py3-pip \
                       python3 &&\
    apk add --no-cache --virtual .build-deps build-base \
                                             git \
                                             libffi-dev \
                                             libressl-dev \
                                             python3-dev &&\
    pip3 install --no-cache-dir --upgrade pip==$PIP_TAG && \
    pip3 install --no-cache-dir -r requirements.txt &&\
    apk del .build-deps

COPY . .

ENTRYPOINT ["python3", "chained_ci_mqtt_trigger.py"]
CMD ["/etc/chained-ci-mqtt-trigger.conf"]
